from xmptools import XMPMetadata, makeFileURI
from rdflib import Graph
import sys

# Read metadata and transform into a simplified graph

def queryAndTransform(xmp_file):
    # read in metadata
    xmp = XMPMetadata(makeFileURI(xmp_file))
    # use a SPARQL CONSTRUCT query to build a new graph
    results = xmp.query("""
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        PREFIX Iptc4xmpCore: <http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/>
        PREFIX crs: <http://ns.adobe.com/camera-raw-settings/1.0/>
        CONSTRUCT { ?file dc:subject ?subject ; dc:creator ?creator ; Iptc4xmpCore:Location ?loc }
        WHERE {
            # get the image file name
            ?xmp crs:RawFileName ?raw .
            # extract dc:subject and dc:creator from containers and make them repeated properties
            OPTIONAL { ?xmp dc:subject [ a rdf:Bag ; !a ?subject ] . }
            OPTIONAL { ?xmp dc:creator [ a rdf:Seq ; !a ?creator ] . }
            # extract location
            OPTIONAL { ?xmp Iptc4xmpCore:Location ?loc . }
            # find the filename extension of the original image file
            BIND (STRAFTER(STR(?raw), ".") AS ?ext)
            # construct a new URI using the image file extension
            # note that we make the tacit assumption that the file has extension "xmp" (length=3)
            BIND (IRI(CONCAT(SUBSTR(STR(?xmp), 1, STRLEN(STR(?xmp))-3), ?ext)) AS ?file)
        }""")
    graph = Graph()
    # insert query results into a new Graph instance
    for statement in results:
        graph.add(statement)
    return graph

g = queryAndTransform("../../tests/test.xmp")
g.serialize(destination=sys.stdout.buffer, format="turtle")
