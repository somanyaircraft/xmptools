from xmptools import XMPMetadata, makeFileURI, DC

# Add one keyword (dc:subject) to the metadata

def addDCSubject(xmp_file, new_subject):
    # read in metadata
    xmp = XMPMetadata(makeFileURI(xmp_file))
    # fetch existing dc:subject keywords
    keywords = xmp.getContainerItems(DC.subject)
    if new_subject not in keywords:
        # set the new list of keywords
        xmp.setContainerItems(DC.subject, keywords + [new_subject])
        # set the new modification date
        xmp.setDate()
        # write the metadata back to the file
        xmp.write()
        print("New XMP file written")
    else:
        print("Keyword already included, no file written")

addDCSubject("../tests/test.xmp", "Boeing 737")
