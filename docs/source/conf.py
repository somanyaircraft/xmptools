project = 'xmptools'
copyright = '2021-2022, Ora Lassila & So Many Aircraft'
author = 'Ora Lassila'
release = '0.6.x'

extensions = ['myst_parser']
templates_path = ['_templates']
exclude_patterns = []
html_theme = 'pyramid'
html_static_path = ['_static']
html_use_index = False
html_copy_source = False
