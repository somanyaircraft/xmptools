import xmptools
import sys
import os.path

image = "./testinput.xmp"
imageurl = "file://" + os.path.realpath(image)
testfile = "./test.xmp"
testfileurl = "file://" + os.path.realpath(testfile)

xmp = xmptools.XMPMetadata(imageurl)
xmp.write(sys.stdout.buffer)
