import unittest
import rdfhelpers
import xmptools.xmptools
from xmptools.xmptools import XMPMetadata, adjustNodes, adjustNodesInPlace, makeFileURI
from xmptools.xmptools import XMP, DC, FileTypeError, timezone
import rdflib
from rdflib.compare import to_isomorphic
import os.path
import shutil
from datetime import date, datetime
import PIL.Image

EX = rdflib.Namespace("https://somanyaircraft.com/example#")

def checkInitialized(xmp, identifier_class):
    return (xmp.initialized and
            (xmp.url is not None)
            and isinstance(xmp.identifier, identifier_class))

def path(suffix):
    return os.path.join(os.path.dirname(__file__), suffix)

class TestXMPReadWrite(unittest.TestCase):

    def setUp(self):
        self.image = path("testinput.xmp")
        self.imageurl = "file://" + os.path.realpath(self.image)
        self.testfile = path("test.xmp")
        self.testfileurl = "file://" + os.path.realpath(self.testfile)

    def tearDown(self):
        try:
            pass
            # os.remove(self.testfile)
        except FileNotFoundError:
            pass

    def test_havefile(self):
        self.assertTrue(os.path.exists(os.path.realpath(self.image)),
                        msg="Test input data not found")

    def test_parse(self):
        xmp = XMPMetadata(self.imageurl)
        self.assertTrue(checkInitialized(xmp, rdflib.URIRef))
        self.assertTrue(xmp.identifier == xmp.url)
        self.assertTrue((rdflib.URIRef(self.imageurl), None, None) in xmp)
        triples = list(xmp.triples((rdflib.URIRef(self.imageurl),
                                    rdflib.URIRef("http://purl.org/dc/elements/1.1/format"),
                                    None)))
        self.assertTrue(triples[0][2].value == "image/x-canon-cr3", msg="Expected triple not found")

    def test_roundtrip(self):
        shutil.copyfile(self.image, self.testfile)
        xmp1 = XMPMetadata(self.testfileurl)
        xmp1.write(self.testfile)
        xmp2 = XMPMetadata(self.testfileurl)
        self.assertTrue(to_isomorphic(xmp1) == to_isomorphic(xmp2), msg="Graphs do not match")

    def test_archive(self):
        shutil.copyfile(self.image, self.testfile)
        xmp1 = XMPMetadata(self.testfileurl)
        os.remove(self.testfile)
        xmp1.archive(self.testfile)
        os.system("rm -f " + self.testfile + " && gunzip " + self.testfile + ".gz")
        xmp2 = XMPMetadata(self.testfileurl)
        self.assertTrue(to_isomorphic(xmp1) == to_isomorphic(xmp2), msg="Graphs do not match")

class TestMediaProXMPReadWrite(unittest.TestCase):

    def setUp(self):
        self.image = path("testinput2.xmp")
        self.imageurl = "file://" + os.path.realpath(self.image)
        self.testfile = path("test2.xmp")
        self.testfileurl = "file://" + os.path.realpath(self.testfile)

    def test_parse(self):
        xmp = XMPMetadata(self.imageurl)
        self.assertTrue(checkInitialized(xmp, rdflib.URIRef))
        self.assertTrue(xmp.identifier == xmp.url)
        self.assertTrue((rdflib.URIRef(self.imageurl), None, None) in xmp)
        triples = list(xmp.triples((rdflib.URIRef(self.imageurl),
                                    rdflib.URIRef("http://ns.adobe.com/photoshop/1.0/City"),
                                    None)))
        self.assertTrue(triples[0][2].value == "Manchester", msg="Expected triple not found")

    def test_roundtrip(self):
        shutil.copyfile(self.image, self.testfile)
        xmp1 = XMPMetadata(self.testfileurl)
        xmp1.write(self.testfile)
        xmp2 = XMPMetadata(self.testfileurl)
        self.assertTrue(to_isomorphic(xmp1) == to_isomorphic(xmp2), msg="Graphs do not match")

def checkSingleValueContainer(case, items, value):
    case.assertIsNotNone(items)
    case.assertTrue(len(items) == 1)
    case.assertTrue(items[0] == value)

def checkTwoValuecontainer(case, items, value1, value2):
    case.assertIsNotNone(items)
    case.assertTrue(len(items) == 2)
    case.assertTrue(items[0] == value1)
    case.assertTrue(items[1] == value2)

class TestSidecarXMPManipulation(unittest.TestCase):

    def setUp(self):
        self.image = path("testinput.xmp")
        self.imageurl = makeFileURI(self.image)
        self.xmp = XMPMetadata(self.imageurl)

    def test_get_nonexistent_statement(self):
        self.assertIsNone(self.xmp.getOneStatement(XMP.foobar))

    def test_get_metadata_date(self):
        d = self.xmp.getDate()
        self.assertIsNotNone(d)
        refdate = datetime.fromisoformat("2021-06-26T16:53:44-04:00")
        self.assertTrue(d == refdate)

    def test_set_metadata_date(self):
        date1 = datetime.now()
        self.xmp.setDate(date1)
        date2 = self.xmp.getDate()
        self.assertTrue(date1 == date2, msg="Dates differ")

    def test_get_metadata_date_with_fractional_seconds(self):
        date1 = self.xmp.getDate(predicate=XMP.CreateDate)
        # Deliberately only two decimal places, something that breaks in the Python Standard Library
        # The following constructs this timestamp: 2021-06-26T08:34:41.47-05:00
        date2 = datetime(2021, 6, 26, 8, 34, 41, 470000, timezone(5, 0, True))
        self.assertTrue(date1 == date2)

    def test_excessive_fractional_precision(self):
        with self.assertRaises(ValueError):
            # Only at most 6 decimal places allowed
            self.xmp.iso8601.parse("2021-06-26T08:34:41.1234567-05:00")

    def test_python_lib_datetime_error(self):
        # The Python Standard Library seems to have a bug in datetime which only accepts either 3
        # decimal places or 6 decimal places; this is not what the ISO 8601 standard says. Once this
        # test no longer passes we can assume that the library has been fixed.
        # See https://github.com/python/cpython/issues/95221
        with self.assertRaises(ValueError,
                               msg="Invalid isoformat string: '2021-06-26T08:34:41.12-05:00'"):
            datetime.fromisoformat("2021-06-26T08:34:41.12-05:00")  # only two decimal places

    def test_find_creation_datetime(self):
        dt, pred = self.xmp.findDateCreated()
        self.assertTrue(isinstance(dt, datetime))
        self.assertTrue(isinstance(pred, rdflib.URIRef))

    def test_find_creation_date(self):
        dt, pred = self.xmp.findDateCreated()
        self.assertTrue(isinstance(dt, date))
        self.assertTrue(isinstance(pred, rdflib.URIRef))

    def test_find_creation_datetime_with_error(self):
        with self.assertRaises(xmptools.xmptools.DataNotFound):
            _, _ = self.xmp.findDateCreated(predicates=[XMP.foo])

    SAMPLE_SPARQL = "SELECT ?value { ?s <http://ns.adobe.com/exif/1.0/ExposureTime> ?value }"

    def test_sparql_access(self):
        results = list(self.xmp.query(self.SAMPLE_SPARQL))
        self.assertTrue(results[0][0].value == "1/1000")

    def test_get_dc_subjects(self):
        checkSingleValueContainer(self, self.xmp.getContainerItems(DC.subject),
                                  rdflib.Literal("Lockheed C-130 Hercules"))

    def test_set_dc_subjects(self):
        self.xmp.setContainerItems(DC.subject,
                                   [rdflib.Literal("Lockheed C-130 Hercules"),
                                    rdflib.Literal("Boeing 737")])
        checkTwoValuecontainer(self, self.xmp.getContainerItems(DC.subject),
                               rdflib.Literal("Lockheed C-130 Hercules"),
                               rdflib.Literal("Boeing 737"))

    def test_get_nonexistent_container(self):
        self.assertIsNone(self.xmp.getContainerItems(XMP.foobar))

    def test_remove_container(self):
        self.assertIsNotNone(self.xmp.getContainerItems(DC.subject))
        self.xmp.setContainerItems(DC.subject, [])
        self.assertIsNone(self.xmp.getContainerItems(DC.subject))
        self.assertIsNone(self.xmp.getOneStatement(DC.subject))

    def test_set_nonexistent_container(self):
        self.xmp.setContainerItems(XMP.foobar,
                                   [rdflib.Literal("foo"), rdflib.Literal("bar")])
        checkTwoValuecontainer(self, self.xmp.getContainerItems(XMP.foobar),
                               rdflib.Literal("foo"), rdflib.Literal("bar"))

class TestJPEGXMPManipulation(unittest.TestCase):

    def setUp(self):
        self.imageurl = os.path.abspath(path("images/testjpeg.jpg"))
        self.xmp = XMPMetadata.fromJPEG(self.imageurl)

    def test_initialized(self):
        self.assertTrue(checkInitialized(self.xmp, rdflib.BNode))

    def test_get_nonexistent_statement(self):
        self.assertIsNone(self.xmp.getOneStatement(XMP.foobar))

    def test_get_dc_subjects(self):
        checkSingleValueContainer(self, self.xmp.getContainerItems(DC.subject),
                                  rdflib.Literal("Northrop A-10 Warthog"))

class TestOtherFileTypes(unittest.TestCase):

    def test_tiff(self):
        xmp = XMPMetadata.fromImageFile(path("images/testtiff.tif"))
        self.assertTrue(checkInitialized(xmp, rdflib.BNode))
        checkSingleValueContainer(self, xmp.getContainerItems(DC.subject),
                                  rdflib.Literal("Antonov An-225"))

    def test_apple_dng(self):
        xmp = XMPMetadata.fromImageFile(path("images/testapple.dng"))
        self.assertTrue(checkInitialized(xmp, rdflib.BNode))
        checkSingleValueContainer(self, xmp.getContainerItems(DC.subject),
                                  rdflib.Literal("de Havilland Canada DHC-8"))
        self.assertTrue(xmp.segment_count == 2)

    def test_nokia_dng(self):
        xmp = XMPMetadata.fromImageFile(path("images/testnokia.dng"))
        self.assertTrue(checkInitialized(xmp, rdflib.BNode))
        checkSingleValueContainer(self, xmp.getContainerItems(DC.subject),
                                  rdflib.Literal("Boeing 757"))
        self.assertTrue(xmp.segment_count == 1)

class TestErrorReporting(unittest.TestCase):

    def checkForFileTypeErrors(self, param):
        with self.assertRaises(FileTypeError):
            XMPMetadata(param)

    def test_badXMP_nonXMP_reporting(self):
        self.checkForFileTypeErrors(path("images/test.cr3"))

    def test_nonJPEG_reporting(self):
        self.checkForFileTypeErrors(path("images/testtiff.tif"))

    def test_failing_XMP_write(self):
        xmp = XMPMetadata.fromJPEG(path("images/testjpeg.jpg"))
        with self.assertRaises(NotImplementedError):
            xmp.write()

class TestGraphAdjustments(unittest.TestCase):

    def setUp(self):
        self.graph = rdflib.Graph()
        self.graph.add((rdflib.RDF.type, rdflib.RDF.type, rdflib.RDFS.Resource))

    def test_node_adjustment(self):
        g = adjustNodes({rdflib.RDF.type: rdflib.RDF.Property,
                         rdflib.RDFS.Resource: rdflib.RDFS.Class},
                        self.graph)
        self.assertTrue(len(g) == 1)
        self.assertTrue(self.graph is not g)
        triples = list(g.triples((rdflib.RDF.Property, rdflib.RDF.type, rdflib.RDFS.Class)))
        self.assertTrue(len(triples) == 1)

    def test_node_adjustment_in_place(self):
        g = adjustNodesInPlace({rdflib.RDF.type: rdflib.RDF.Property,
                                rdflib.RDFS.Resource: rdflib.RDFS.Class},
                               self.graph)
        self.assertTrue(self.graph is g)
        triples = list(self.graph.triples((rdflib.RDF.Property,
                                           rdflib.RDF.type,
                                           rdflib.RDFS.Class)))
        self.assertTrue(len(triples) == 1)

    def test_node_adjustment_with_predicates(self):
        g = adjustNodes({rdflib.RDF.type: rdflib.RDFS.range}, self.graph, check_predicates=True)
        self.assertTrue(len(g) == 1)
        triples = list(g.triples((rdflib.RDFS.range, rdflib.RDFS.range, rdflib.RDFS.Resource)))
        self.assertTrue(len(triples) == 1)

    def test_literal_adjustment(self):
        self.graph.add((rdflib.RDF.type, rdflib.RDFS.label, rdflib.Literal("type")))
        g = adjustNodes({rdflib.Literal("type"): rdflib.Literal("foo")}, self.graph)
        self.assertTrue(len(g) == 2)
        triples = list(g.triples((rdflib.RDF.type, rdflib.RDFS.label, None)))
        self.assertTrue(len(triples) == 1)
        self.assertTrue(triples[0][2].value == "foo")

    def test_image_uri_adjustment(self):
        old_xmp = XMPMetadata(makeFileURI(path("testinput.xmp")))
        new_xmp = old_xmp.adjustImageURI()
        self.assertTrue(len(old_xmp) == len(new_xmp))
        self.assertTrue(new_xmp.url == rdflib.URIRef(makeFileURI(path("testinput.cr3"))))

    def test_image_uri_adjustment_in_place(self):
        xmp = XMPMetadata(makeFileURI(path("testinput.xmp")))
        old_uri = xmp.url
        old_len = len(xmp)
        xmp.adjustImageURI(destination_graph=xmp)
        self.assertTrue(len(xmp) == old_len)
        self.assertTrue(xmp.url != old_uri)
        self.assertTrue(xmp.url.endswith("cr3"))

    def test_image_uri_adjustment_with_mapper(self):
        old_xmp = XMPMetadata(makeFileURI(path("testinput.xmp")))
        new_xmp = old_xmp.adjustImageURI(uri_mapper=lambda u: u.replace("testinput", "foobar"))
        self.assertTrue(len(old_xmp) == len(new_xmp))
        self.assertTrue(new_xmp.url == rdflib.URIRef(makeFileURI(path("foobar.cr3"))))

class TestGraphHelpers(unittest.TestCase):

    def setUp(self):
        self.xmp = XMPMetadata(makeFileURI(path("testinput.xmp")))

    def test_cdb_exists(self):
        self.assertTrue(len(self.xmp) > 0)
        self.assertTrue(len(self.xmp.cbd()) == len(self.xmp))  # CBD should be the entire XMP

    def test_cdb_contains_all_necessary_triples(self):
        self.assertTrue(len(self.xmp - self.xmp.cbd()) == 0)

    def test_cdb_excludes_unnecessary_triples(self):
        new_xmp = XMPMetadata(makeFileURI(path("testinput.xmp")))
        new_xmp.add((rdflib.URIRef("https://www.somanyaircraft.com/"),
                     rdflib.RDFS.label,
                     rdflib.Literal("So Many Aircraft")))
        self.assertTrue(len(new_xmp) - len(new_xmp.cbd()) == 1)

    def test_good_xmp_equals_cdb(self):
        xmp2 = XMPMetadata(makeFileURI(path("testinput2.xmp")))
        # Merge two XMP graphs, then compute the CBD of the root of the second XMP graph. This
        # should be the same as the original second XMP graph.
        c = rdfhelpers.cbd(self.xmp + xmp2, xmp2.url, target=rdflib.Graph(), context=None)
        self.assertTrue(to_isomorphic(xmp2) == to_isomorphic(c))

    def test_container2repeated(self):
        g = self.xmp.container2repeated(DC.subject, new_predicate=EX.subject, target_graph=rdflib.Graph())
        self.assertTrue(len(g) == 1)
        s, p, o = next(g.triples((None, None, None)))
        self.assertTrue(p == EX.subject)
        self.assertTrue(s == self.xmp.url)
        self.assertTrue(o == rdflib.Literal("Lockheed C-130 Hercules"))
        self.assertTrue(to_isomorphic(self.xmp)
                        == to_isomorphic(XMPMetadata(makeFileURI(path("testinput.xmp")))))

    def test_container2repeated_in_place(self):
        xmp = XMPMetadata(makeFileURI(path("testinput.xmp")))
        xmp.container2repeated(DC.subject, new_predicate=EX.subject)
        self.assertTrue(xmp.getValue(EX.subject) == rdflib.Literal("Lockheed C-130 Hercules"))
        self.assertTrue(len(xmp) == len(self.xmp) + 1)

    def test_container2repeated_in_place_desctructive(self):
        xmp = XMPMetadata(makeFileURI(path("testinput.xmp")))
        xmp.container2repeated(DC.subject, new_predicate=EX.subject, remove_predicate=True)
        self.assertTrue(xmp.getValue(EX.subject) == rdflib.Literal("Lockheed C-130 Hercules"))
        self.assertTrue(len(xmp) == len(self.xmp) - 2)
        self.assertIsNone(xmp.getValue(DC.subject))

    def test_repeated2container(self):
        xmp = XMPMetadata()  # empty graph
        xmp.url = EX.Foo
        xmp.add((EX.Foo, EX.subject, rdflib.Literal("foo")))
        xmp.repeated2container(EX.subject, DC.subject)
        self.assertTrue(len(xmp) == 4)
        container = xmp.getValue(DC.subject)
        self.assertTrue(next(xmp.objects(container, rdflib.RDF.type)) == rdflib.RDF.Seq)
        self.assertTrue(next(xmp.objects(container,
                                         rdflib.URIRef(rdfhelpers.makeContainerItemPredicate(1))))
                        == rdflib.Literal("foo"))

class TestPDFs(unittest.TestCase):

    def setUp(self) -> None:
        self.xmp, _ = XMPMetadata.fromFile(path("test.pdf"), ignore_sidecar_if_pdf=True)

    def test_PDF_read(self):
        self.assertTrue(len(self.xmp) == 531)

    def test_PDF_thumbnails(self):
        thumbnails = self.xmp.getThumbnails()
        self.assertTrue(thumbnails and len(thumbnails) == 1)
        self.assertTrue(isinstance(thumbnails[0], PIL.Image.Image))

class TestModificationDates(unittest.TestCase):

    def setUp(self) -> None:
        self.image_no_xmp_mod = datetime.fromtimestamp(os.path.getmtime(path("images/test.cr3")))
        self.xmp_no_image_mod = datetime.fromtimestamp(os.path.getmtime(path("testinput.xmp")))
        self.image_mod = datetime.fromtimestamp(os.path.getmtime(path("images/20230924-132602-280.CR3")))
        self.xmp_mod = datetime.fromtimestamp(os.path.getmtime(path("images/20230924-132602-280.xmp")))

    def test_image_date_no_xmp(self):
        self.assertTrue(XMPMetadata.fileModificationDate(path("images/test.cr3")) \
                        == self.image_no_xmp_mod)

    def test_image_date(self):
        self.assertTrue(XMPMetadata.fileModificationDate(path("images/20230924-132602-280.CR3")) \
                        == self.xmp_mod)
        self.assertFalse(XMPMetadata.fileModificationDate(path("images/20230924-132602-280.CR3")) \
                        == self.image_mod)

    def test_xmp_date(self):
        self.assertTrue(XMPMetadata.fileModificationDate(path("testinput.xmp")) \
                        == self.xmp_no_image_mod)
